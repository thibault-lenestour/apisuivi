<?php

namespace Api\SuiviBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TacheSprint
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Api\SuiviBundle\Entity\TacheSprintRepository")
 */
class TacheSprint{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Tache")
     * @ORM\JoinColumn(name="tache", referencedColumnName="id")
     */
    private $tache;
    
    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Sprint")
     * @ORM\JoinColumn(name="sprint", referencedColumnName="id")
     */
    private $sprint;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }
    
    public function getTache(){
        return $this->tache;
    }

    public function getSprint(){
        return $this->sprint;
    }

    public function setTache($tache){
        $this->tache = $tache;
        
        return $this;
    }

    public function setSprint($sprint){
        $this->sprint = $sprint;
        
        return $this;
    }


}
