<?php

namespace Api\SuiviBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Roadmap
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Api\SuiviBundle\Entity\RoadmapRepository")
 */
class Roadmap{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var integer
     *
     * @ORM\Column(name="dateDebut", type="integer")
     */
    private $dateDebut;

    /**
     * @var integer
     *
     * @ORM\Column(name="dateFin", type="integer")
     */
    private $dateFin;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Roadmap
     */
    public function setNom($nom){
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom(){
        return $this->nom;
    }

    /**
     * Set dateDebut
     *
     * @param integer $dateDebut
     * @return Roadmap
     */
    public function setDateDebut($dateDebut){
        $date = date("U",strtotime($dateDebut)); 
        $this->dateDebut = $date;
        
        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return date 
     */
    public function getDateDebut(){
        if($this->dateDebut != 0){
            $date = date("d F y",$this->dateDebut); 
        }else{
            $date = "";
        }
        return $date;
    }

    /**
     * Set dateFin
     *
     * @param integer $dateFin
     * @return Roadmap
     */
    public function setDateFin($dateFin){
        $date = date("U",strtotime($dateFin));
        $this->dateFin = $date;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return integer 
     */
    public function getDateFin(){
        if($this->dateFin != 0){
            $date = date("d F y",$this->dateFin); 
        }else{
            $date = "";
        } 
        return $date;
    }
    
        
   /**
    * @return string
    */
    public function __toString(){
		return $this->getNom()." - ".$this->getDateDebut()." / ".$this->getDateFin();
    }
}
