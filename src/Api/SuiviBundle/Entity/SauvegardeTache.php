<?php

namespace Api\SuiviBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SauvegardeTache
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Api\SuiviBundle\Entity\SauvegardeTacheRepository")
 */
class SauvegardeTache{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="tps_original", type="float")
     */
    private $tpsOriginal;

    /**
     * @var float
     *
     * @ORM\Column(name="tps_passe", type="float")
     */
    private $tpsPasse;

    /**
     * @var float
     *
     * @ORM\Column(name="tps_restant", type="float")
     */
    private $tpsRestant;

    /**
     * @var string
     *
     * @ORM\Column(name="v_affectee", type="string", length=255)
     */
    private $vAffectee;

    /**
     * @var string
     *
     * @ORM\Column(name="v_resolue", type="string", length=255)
     */
    private $vResolue;

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string", length=255)
     */
    private $etat;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="alerte", type="string", length=255)
     */
    private $alerte;

    /**
     * @var integer
     *
     * @ORM\Column(name="date", type="integer")
     */
    private $date;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Tache")
     * @ORM\JoinColumn(name="tache", referencedColumnName="id")
     */
    private $tache;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set tpsOriginal
     *
     * @param float $tpsOriginal
     * @return SauvegardeTache
     */
    public function setTpsOriginal($tpsOriginal){
        $this->tpsOriginal = $tpsOriginal;

        return $this;
    }

    /**
     * Get tpsOriginal
     *
     * @return float 
     */
    public function getTpsOriginal(){
        return $this->tpsOriginal;
    }

    /**
     * Set tpsPasse
     *
     * @param float $tpsPasse
     * @return SauvegardeTache
     */
    public function setTpsPasse($tpsPasse){
        $this->tpsPasse = $tpsPasse;

        return $this;
    }

    /**
     * Get tpsPasse
     *
     * @return float 
     */
    public function getTpsPasse(){
        return $this->tpsPasse;
    }

    /**
     * Set tpsRestant
     *
     * @param float $tpsRestant
     * @return SauvegardeTache
     */
    public function setTpsRestant($tpsRestant){
        $this->tpsRestant = $tpsRestant;

        return $this;
    }

    /**
     * Get tpsRestant
     *
     * @return float 
     */
    public function getTpsRestant(){
        return $this->tpsRestant;
    }

    /**
     * Set vAffectee
     *
     * @param string $vAffectee
     * @return SauvegardeTache
     */
    public function setVAffectee($vAffectee){
        $this->vAffectee = $vAffectee;

        return $this;
    }

    /**
     * Get vAffectee
     *
     * @return string 
     */
    public function getVAffectee(){
        return $this->vAffectee;
    }

    /**
     * Set vResolue
     *
     * @param string $vResolue
     * @return SauvegardeTache
     */
    public function setVResolue($vResolue){
        $this->vResolue = $vResolue;

        return $this;
    }

    /**
     * Get vResolue
     *
     * @return string 
     */
    public function getVResolue(){
        return $this->vResolue;
    }

    /**
     * Set etat
     *
     * @param string $etat
     * @return SauvegardeTache
     */
    public function setEtat($etat){
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string 
     */
    public function getEtat(){
        return $this->etat;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return SauvegardeTache
     */
    public function setColor($color){
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor(){
        return $this->color;
    }

    /**
     * Set alerte
     *
     * @param string $alerte
     * @return SauvegardeTache
     */
    public function setAlerte($alerte){
        $this->alerte = $alerte;

        return $this;
    }

    /**
     * Get alerte
     *
     * @return string 
     */
    public function getAlerte(){
        return $this->alerte;
    }

    /**
     * Set date
     *
     * @param integer $date
     * @return SauvegardeTache
     */
    public function setDate($date){
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return integer 
     */
    public function getDate(){
        return $this->date;
    }

    /**
     * Set tache
     *
     * @param string $tache
     * @return SauvegardeTache
     */
    public function setTache($tache){
        $this->tache = $tache;

        return $this;
    }

    /**
     * Get tache
     *
     * @return string 
     */
    public function getTache(){
        return $this->tache;
    }

}
