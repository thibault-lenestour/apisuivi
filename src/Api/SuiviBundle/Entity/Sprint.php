<?php

namespace Api\SuiviBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sprint
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Api\SuiviBundle\Entity\SprintRepository")
 */
class Sprint{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=255)
     */
    private $version;

    /**
     * @var integer
     *
     * @ORM\Column(name="dateDebut", type="integer")
     */
    private $dateDebut;

    /**
     * @var integer
     *
     * @ORM\Column(name="dateFin", type="integer")
     */
    private $dateFin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enCours", type="boolean")
     */
    private $enCours;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Roadmap")
     * @ORM\JoinColumn(name="roadmap", referencedColumnName="id")
     */
    private $roadmap;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Projet")
     * @ORM\JoinColumn(name="projet", referencedColumnName="id")
     */
    private $projet;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set version
     *
     * @param string $version
     * @return Sprint
     */
    public function setVersion($version){
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string 
     */
    public function getVersion(){
        return $this->version;
    }

    /**
     * Set dateDebut
     *
     * @param integer $dateDebut
     * @return Roadmap
     */
    public function setDateDebut($dateDebut){
        $date = date("U",strtotime($dateDebut)); 
        $this->dateDebut = $date;
        
        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return date 
     */
    public function getDateDebut(){
        if($this->dateDebut != 0){
            $date = date("d F y",$this->dateDebut); 
        }else{
            $date = "";
        }
        return $date;
    }

    /**
     * Set dateFin
     *
     * @param integer $dateFin
     * @return Roadmap
     */
    public function setDateFin($dateFin){
        $date = date("U",strtotime($dateFin));
        $this->dateFin = $date;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return integer 
     */
    public function getDateFin(){
        if($this->dateFin != 0){
            $date = date("d F y",$this->dateFin); 
        }else{
            $date = "";
        } 
        return $date;
    }

    /**
     * Set enCours
     *
     * @param boolean $enCours
     * @return Sprint
     */
    public function setEnCours($enCours){
        $this->enCours = $enCours;

        return $this;
    }

    /**
     * Get enCours
     *
     * @return boolean 
     */
    public function getEnCours(){
        return $this->enCours;
    }

    /**
     * Set roadmap
     *
     * @param string $roadmap
     * @return Sprint
     */
    public function setRoadmap($roadmap){
        $this->roadmap = $roadmap;

        return $this;
    }

    /**
     * Get roadmap
     *
     * @return string 
     */
    public function getRoadmap(){
        return $this->roadmap;
    }

    /**
     * Set projet
     *
     * @param string $projet
     * @return Sprint
     */
    public function setProjet($projet){
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return string 
     */
    public function getProjet(){
        return $this->projet;
    }
    
   /**
    * @return string
    */
    public function __toString(){
		return $this->getProjet()." - ".$this->getVersion();
    }
}
