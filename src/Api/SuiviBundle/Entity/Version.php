<?php

namespace Api\SuiviBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Version
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Api\SuiviBundle\Entity\VersionRepository")
 */
class Version{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=255)
     */
    private $version;
    
    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Tache")
     * @ORM\JoinColumn(name="tache", referencedColumnName="id")
     */
    private $tache;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set version
     *
     * @param string $version
     * @return Version
     */
    public function setVersion($version){
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return string 
     */
    public function getVersion(){
        return $this->version;
    }
    
    public function getTache(){
        return $this->tache;
    }

    public function setTache($tache){
        $this->tache = $tache;
        
        return $this;
    }


}
