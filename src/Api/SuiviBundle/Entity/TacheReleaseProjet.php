<?php

namespace Api\SuiviBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TacheReleaseProjet
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Api\SuiviBundle\Entity\TacheReleaseProjetRepository")
 */
class TacheReleaseProjet{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Tache")
     * @ORM\JoinColumn(name="tache", referencedColumnName="id")
     */
    private $tache;
    
    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="ReleaseProjet")
     * @ORM\JoinColumn(name="releaseProjet", referencedColumnName="id")
     */
    private $releaseProjet;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }
    
    public function getTache(){
        return $this->tache;
    }

    public function getReleaseProjet(){
        return $this->releaseProjet;
    }

    public function setTache($tache){
        $this->tache = $tache;
        
        return $this;
    }

    public function setReleaseProjet($releaseProjet){
        $this->releaseProjet = $releaseProjet;
        
        return $this;
    }
}
