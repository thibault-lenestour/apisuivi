<?php

namespace Api\SuiviBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SauvegardeRelease
 *
 * @ORM\Table(name="SauvegardeRelease")
 * @ORM\Entity(repositoryClass="Api\SuiviBundle\Entity\SauvegardeReleaseRepository")
 */
class SauvegardeRelease{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="tps_original", type="float")
     */
    private $tpsOriginal;

    /**
     * @var float
     *
     * @ORM\Column(name="tps_passe", type="float")
     */
    private $tpsPasse;

    /**
     * @var float
     *
     * @ORM\Column(name="tps_restant", type="float")
     */
    private $tpsRestant;

    /**
     * @var integer
     *
     * @ORM\Column(name="date", type="integer")
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="ajout", type="integer")
     */
    private $ajout;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="ReleaseProjet")
     * @ORM\JoinColumn(name="releaseProjet", referencedColumnName="id")
     */
    private $releaseProjet;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set tpsOriginal
     *
     * @param float $tpsOriginal
     * @return SauvegardeRelease
     */
    public function setTpsOriginal($tpsOriginal){
        $this->tpsOriginal = $tpsOriginal;

        return $this;
    }

    /**
     * Get tpsOriginal
     *
     * @return float 
     */
    public function getTpsOriginal(){
        return $this->tpsOriginal;
    }

    /**
     * Set tpsPasse
     *
     * @param float $tpsPasse
     * @return SauvegardeRelease
     */
    public function setTpsPasse($tpsPasse){
        $this->tpsPasse = $tpsPasse;

        return $this;
    }

    /**
     * Get tpsPasse
     *
     * @return float 
     */
    public function getTpsPasse(){
        return $this->tpsPasse;
    }

    /**
     * Set tpsRestant
     *
     * @param float $tpsRestant
     * @return SauvegardeRelease
     */
    public function setTpsRestant($tpsRestant){
        $this->tpsRestant = $tpsRestant;

        return $this;
    }

    /**
     * Get tpsRestant
     *
     * @return float 
     */
    public function getTpsRestant(){
        return $this->tpsRestant;
    }

    /**
     * Set date
     *
     * @param integer $date
     * @return SauvegardeRelease
     */
    public function setDate($date){
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return integer 
     */
    public function getDate(){
        return $this->date;
    }

    /**
     * Set ajout
     *
     * @param integer $ajout
     * @return SauvegardeRelease
     */
    public function setAjout($ajout){
        $this->ajout = $ajout;

        return $this;
    }

    /**
     * Get ajout
     *
     * @return integer 
     */
    public function getAjout(){
        return $this->ajout;
    }

    /**
     * Set release
     *
     * @param string $releaseProjet
     * @return SauvegardeRelease
     */
    public function setReleaseProjet($releaseProjet){
        $this->releaseProjet = $releaseProjet;

        return $this;
    }

    /**
     * Get release
     *
     * @return string 
     */
    public function getReleaseProjet(){
        return $this->releaseProjet;
    }
}
