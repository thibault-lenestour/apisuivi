<?php

namespace Api\SuiviBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Release
 *
 * @ORM\Table(name="ReleaseProjet")
 * @ORM\Entity(repositoryClass="Api\SuiviBundle\Entity\ReleaseProjetRepository")
 */
class ReleaseProjet{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="requete", type="text")
     */
    private $requete;
    
    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Projet")
     * @ORM\JoinColumn(name="projet", referencedColumnName="id")
     */
    private $projet;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Roadmap")
     * @ORM\JoinColumn(name="roadmap", referencedColumnName="id")
     */
    private $roadmap;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return ReleaseProjet
     */
    public function setNom($nom){
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom(){
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ReleaseProjet
     */
    public function setDescription($description){
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription(){
        return $this->description;
    }

    /**
     * Set requete
     *
     * @param string $requete
     * @return ReleaseProjet
     */
    public function setRequete($requete){
        $this->requete = $requete;

        return $this;
    }

    /**
     * Get requete
     *
     * @return string 
     */
    public function getRequete(){
        return $this->requete;
    }
    
    public function getProjet(){
        return $this->projet;
    }

    public function getRoadmap(){
        return $this->roadmap;
    }

    public function setProjet($projet){
        $this->projet = $projet;
        
        return $this;
    }

    public function setRoadmap($roadmap){
        $this->roadmap = $roadmap;
        
        return $this;
    }
    
   /**
    * @return string
    */
    public function __toString(){
		return $this->getNom();
    }
}
