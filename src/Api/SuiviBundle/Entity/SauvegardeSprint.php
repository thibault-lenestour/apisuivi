<?php

namespace Api\SuiviBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SauvegardeSprint
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Api\SuiviBundle\Entity\SauvegardeSprintRepository")
 */
class SauvegardeSprint{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="ajout", type="integer")
     */
    private $ajout;

    /**
     * @var float
     *
     * @ORM\Column(name="tps_original", type="float")
     */
    private $tpsOriginal;

    /**
     * @var float
     *
     * @ORM\Column(name="tps_passe", type="float")
     */
    private $tpsPasse;

    /**
     * @var float
     *
     * @ORM\Column(name="tps_restant", type="float")
     */
    private $tpsRestant;

    /**
     * @var integer
     *
     * @ORM\Column(name="date", type="integer")
     */
    private $date;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Sprint")
     * @ORM\JoinColumn(name="sprint", referencedColumnName="id")
     */
    private $sprint;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set tpsOriginal
     *
     * @param float $tpsOriginal
     * @return SauvegardeSprint
     */
    public function setTpsOriginal($tpsOriginal){
        $this->tpsOriginal = $tpsOriginal;

        return $this;
    }

    /**
     * Get tpsOriginal
     *
     * @return float 
     */
    public function getTpsOriginal(){
        return $this->tpsOriginal;
    }

    /**
     * Set tpsPasse
     *
     * @param float $tpsPasse
     * @return SauvegardeSprint
     */
    public function setTpsPasse($tpsPasse){
        $this->tpsPasse = $tpsPasse;

        return $this;
    }

    /**
     * Get tpsPasse
     *
     * @return float 
     */
    public function getTpsPasse(){
        return $this->tpsPasse;
    }

    /**
     * Set tpsRestant
     *
     * @param float $tpsRestant
     * @return SauvegardeSprint
     */
    public function setTpsRestant($tpsRestant){
        $this->tpsRestant = $tpsRestant;

        return $this;
    }

    /**
     * Get tpsRestant
     *
     * @return float 
     */
    public function getTpsRestant(){
        return $this->tpsRestant;
    }

    /**
     * Set date
     *
     * @param integer $date
     * @return SauvegardeSprint
     */
    public function setDate($date){
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return integer 
     */
    public function getDate(){
        return $this->date;
    }

    /**
     * Set sprint
     *
     * @param string $sprint
     * @return SauvegardeSprint
     */
    public function setSprint($sprint){
        $this->sprint = $sprint;

        return $this;
    }

    /**
     * Get sprint
     *
     * @return string 
     */
    public function getSprint(){
        return $this->sprint;
    }
    
    public function getAjout() {
        return $this->ajout;
    }

    public function setAjout($ajout) {
        $this->ajout = $ajout;
        
        return $this;
    }


}
