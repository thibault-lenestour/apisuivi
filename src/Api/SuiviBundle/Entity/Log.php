<?php

namespace Api\SuiviBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Log
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Api\SuiviBundle\Entity\LogRepository")
 */
class Log{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="date", type="integer")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="typeSave", type="string", length=45)
     */
    private $typeSave;

    /**
     * @var string
     *
     * @ORM\Column(name="trace", type="text")
     */
    private $trace;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set date
     *
     * @param integer $date
     * @return Log
     */
    public function setDate($date){
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return integer 
     */
    public function getDate(){
        return $this->date;
    }

    /**
     * Set typeSave
     *
     * @param string $typeSave
     * @return Log
     */
    public function setTypeSave($typeSave){
        $this->typeSave = $typeSave;

        return $this;
    }

    /**
     * Get typeSave
     *
     * @return string 
     */
    public function getTypeSave(){
        return $this->typeSave;
    }

    /**
     * Set trace
     *
     * @param string $trace
     * @return Log
     */
    public function setTrace($trace){
        $this->trace = $trace;

        return $this;
    }

    /**
     * Get trace
     *
     * @return string 
     */
    public function getTrace(){
        return $this->trace;
    }
    
        
   /**
    * @return string
    */
    public function __toString(){
		return "Sauvegarde ".$this->getTypeSave()." du ".$this->getDate();
    }

}
