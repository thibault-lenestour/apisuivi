<?php

namespace Api\SuiviBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tache
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Api\SuiviBundle\Entity\TacheRepository")
 */
class Tache{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="clef", type="string", length=45)
     */
    private $clef;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="v_affectee", type="string", length=45)
     */
    private $vAffectee;

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string", length=255)
     */
    private $etat;
    
    /**
     * @var string
     *
     * @ORM\Column(name="typeIssue", type="string", length=45)
     */
    private $issueType;
    
    /**
     * @var string
     *
     * @ORM\Column(name="personne", type="string", length=255)
     */
    private $personne;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set clef
     *
     * @param string $clef
     * @return Tache
     */
    public function setClef($clef){
        $this->clef = $clef;

        return $this;
    }

    /**
     * Get clef
     *
     * @return string 
     */
    public function getClef(){
        return $this->clef;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Tache
     */
    public function setDescription($description){
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription(){
        return $this->description;
    }

    /**
     * Set vAffectee
     *
     * @param string $vAffectee
     * @return Tache
     */
    public function setVAffectee($vAffectee){
        $this->vAffectee = $vAffectee;

        return $this;
    }

    /**
     * Get vAffectee
     *
     * @return string 
     */
    public function getVAffectee(){
        return $this->vAffectee;
    }

    /**
     * Set etat
     *
     * @param string $etat
     * @return Tache
     */
    public function setEtat($etat){
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string 
     */
    public function getEtat(){
        return $this->etat;
    }
    
    public function getIssueType(){
        return $this->issueType;
    }

    public function getPersonne(){
        return $this->personne;
    }

    public function setIssueType($issueType){
        $this->issueType = $issueType;
        
        return $this;
    }

    public function setPersonne($personne){
        $this->personne = $personne;
        
        return $this;
    }
        
   /**
    * @return string
    */
    public function __toString(){
		return $this-> getClef()." ".$this->getDescription();
    }
    
}
