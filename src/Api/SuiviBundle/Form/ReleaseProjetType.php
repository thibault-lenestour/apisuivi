<?php

namespace Api\SuiviBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ReleaseProjetType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text', array('label' => 'Nom'))
                ->add('description', 'textarea', array('label' => 'Description'))
                ->add('requete', 'textarea', array('label' => 'Requête sur les JIRAs'))
                ->add('roadmap', 'entity', array(
                    'class' => 'ApiSuiviBundle:Roadmap',
                    'query_builder' => function(EntityRepository $er) {
                //Roadmap order by date (lastest to oldest) - Lastest by default
                return $er->createQueryBuilder('roadmap')->orderBy('roadmap.dateDebut', 'DESC');
            }
                        )
                )
                ->add('projet', 'entity', array(
                    'class' => 'ApiSuiviBundle:Projet',
                    'query_builder' => function(EntityRepository $er) {
                //Project order by name - No default value
                return $er->createQueryBuilder('projet')->orderBy('projet.nom', 'ASC');
            },
                    'empty_value' => 'Choisissez un projet',
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Api\SuiviBundle\Entity\ReleaseProjet'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'api_suivibundle_release';
    }

}
