<?php

namespace Api\SuiviBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LogType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('date', 'text', array('label' => 'Date et heure de sauvegarde'))
                ->add('typeSave', 'text', array('label' => 'Type de sauvegarde'))
                ->add('trace', 'text', array('label' => 'Trace'))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Api\SuiviBundle\Entity\Log'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'api_suivibundle_log';
    }

}
