<?php

namespace Api\SuiviBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RoadmapType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text', array('label' => 'Nom'))
                ->add('dateDebut', 'text', array('label' => 'Date de début'))
                ->add('dateFin', 'text', array('label' => 'Date de fin'))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Api\SuiviBundle\Entity\Roadmap'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'api_suivibundle_roadmap';
    }

}
