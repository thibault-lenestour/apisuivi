<?php

namespace Api\SuiviBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UtilisateurType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('username', 'text', array('label' => 'Identifiant de connexion'))
                ->add('nom', 'text', array('label' => 'Nom'))
                ->add('prenom', 'text', array('label' => 'Prénom'))
                ->add('email', 'text', array('label' => 'Email'))
                // No display for crypted password
                ->add('password', 'text', array('label' => 'Nouveau mot de passe', 'data' => ''))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Api\SuiviBundle\Entity\Utilisateur'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'api_suivibundle_utilisateur';
    }

}
