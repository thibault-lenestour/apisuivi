<?php

namespace Api\SuiviBundle\Features\Context;

use Symfony\Component\HttpKernel\KernelInterface;
use Behat\Symfony2Extension\Context\KernelAwareInterface;
use Behat\MinkExtension\Context\MinkContext;

use Behat\Behat\Context\BehatContext,
    Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

require_once 'PHPUnit/Autoload.php';
require_once 'PHPUnit/Framework/Assert/Functions.php';

/**
 * Feature context.
 */
class FeatureContext extends MinkContext implements KernelAwareInterface{
    private $kernel;
    private $parameters;

    /**
     * Initializes context with parameters from behat.yml.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters){
        $this->parameters = $parameters;
    }

    /**
     * Sets HttpKernel instance.
     * This method will be automatically called by Symfony2Extension ContextInitializer.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel){
        $this->kernel = $kernel;
    }
	
	public function jqueryWait($duration = 1000)
    {
        $this->getSession()->wait($duration, '');
    }

    /**
	* @Then /^I should see the modal "([^"]*)"$/
	*/
    public function iShouldSeeTheModal($arg1)
    {
         $message = $this->getSession()->getDriver()->getWebDriverSession()->getAlert_text();
         assertEquals($message, "Erreur identifiant/mot de passe");
         
         $this->getSession()->getDriver()->getWebDriverSession()->accept_alert();
    }
    
    /**
     * @Given /^I am connected with "([^"]*)" and "([^"]*)" on "([^"]*)"$/
     */
    public function iAmConnectedWithAndOn($login, $password, $url)
    {
        $this->getSession()->visit($this->locatePath('/'));
        $this->fillField('_username', $login);
        $this->fillField('_password', $password);
        $this->pressButton('login');

        $this->visit($url);
    }
    
   /**
    * @Given /^I clean projet creation "([^"]*)"$/
    */
    public function iCleanProjetCreation($name){
        $entityManager = $this->getEm();
        
        $projet = $entityManager->getRepository('ApiSuiviBundle:Projet')->findOneByNom($name);
        $entityManager->remove($projet);
        $entityManager->flush();
    }
    
    /**
     * @Given /^I should see a div named "([^"]*)"$/
     */
    public function iShouldSeeADivNamed($arg1)
    {
        $element = $this->getSession()->getPage()->findById($arg1);
        assertNotNull($element);
        
    }
    
     public function getEm(){
          return $this->getContainer()->get('doctrine')->getManager();
     }

     /**
      * gets container from kernel
      *
      * @return \Symfony\Component\DependencyInjection\ContainerInterface
      */
     protected function getContainer()
     {
         return $this->kernel->getContainer();
     }

}
