Feature: In order to know which are the project
  As a user
  I need to see a list of projects

@javascript
Scenario: Fail Admin Connexion
  Given I am on homepage
  When I fill in "_username" with "administrateur"
  And I fill in "_password" with "admin"
  And I press "login"
  Then I should see the modal "Erreur identifiant/mot de passe"

@javascript
Scenario: Admin Connexion
  Given I am on homepage
  When I fill in "_username" with "administrateur"
  And I fill in "_password" with "R0admap$"
  And I press "login"
  Then I should see "Compte"
  When I press "Se déconnecter"
  Then I should not see "Compte"

@javascript
Scenario: Go to projects list and see it
  Given I am on homepage
  When I follow "projet"
  Then I should be on "/projet/"
  And I should see "Liste des projets"
  And I should see an "table" element

@javascript
Scenario: Create a project
  Given I am connected with "administrateur" and "R0admap$" on "/projet/"
  When I press "api_suivibundle_add_projet"
  Then I should see an "form" element
  When I fill in "api_suivibundle_projet_nom" with "Projet Test"
  And I fill in "api_suivibundle_projet_description" with "Ceci est un projet de test"
  And I fill in "api_suivibundle_projet_code" with "TEST"
  And I press "api_suivibundle_projet_submit"
  Then I should be on "/projet/"
  And I should see "Projet Test"
  And I clean projet creation "Projet Test"
  
@javascript
Scenario: Consult a task historic
  Given I am on homepage
  When I press "historique_Offiref"
  Then I should see "Historique du release Offiref"
  And I should see a div named "containerGraph"
  When I press "Histogramme"
  Then I should see "Release Offiref"
  And I should see a div named "chart_div"
  When I press "Liste des taches"
  Then I should see an "table" element
  And I should see "WREF-38"
  When I follow "tache_WREF-38"
  Then I should see "Historique de la tache WREF-38"
  And I should see a div named "containerGraph"