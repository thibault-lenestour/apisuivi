<?php

namespace Api\SuiviBundle\Controller;

use Api\SuiviBundle\Entity\SauvegardeRelease;
use Api\SuiviBundle\Entity\Tache;
use Api\SuiviBundle\Entity\SauvegardeTache;
use Api\SuiviBundle\Entity\Sprint;
use Api\SuiviBundle\Entity\TacheSprint;
use Api\SuiviBundle\Entity\ReleaseProjet;
use Api\SuiviBundle\Entity\TacheReleaseProjet;
use Api\SuiviBundle\Entity\SauvegardeSprint;
use Api\SuiviBundle\Entity\Version;
use Api\SuiviBundle\Controller\Jira\Api;
use Api\SuiviBundle\Controller\Jira\Api\Authentication\Basic;
use Api\SuiviBundle\Controller\Jira\Issues\Walker;

class SaveCenter {

    /**
     * Save tasks linked to a release
     * Save a SauvegardeRelease
     * Save a line for each link in the join table between release and task
     * 
     * @param ReleaseProjet $release
     * @param integer $date
     * @param EntityManager $entityManager
     * @return SauvegardeRelease
     */
    public function saveReleaseTasks($release, $date, $entityManager) {
        //Get request
        $request = $release->getRequete();

        // Create connexion and make the request
        $api = new Api("http://agora.villers.pharmagest.com/jira", new Basic('scrochard', 'SeB1;WebC7'));
        $walker = new Walker($api);
        $walker->push($request);

        $originalTime = 0;
        $spentTime = 0;
        $remainedTime = 0;

        // Treat each issue from the request answer
        $entitiesAdded = 0;
        foreach ($walker as $issue) {
            $taskSave = $this->treatTask($issue, $release, $entityManager);
            $originalTime += $taskSave->getTpsOriginal();
            $spentTime += $taskSave->getTpsPasse();
            $remainedTime += $taskSave->getTpsRestant();
            $entitiesAdded++;
        }

        // Save a SauvegardeRelease
        return $this->saveSauvegardeRelease($entityManager, $entitiesAdded, $date, $release, $originalTime, $spentTime, $remainedTime);
    }

    /**
     * Save tasks linked to a sprint
     * Save a SauvegardeSprint
     * Save a line for each link in the join table between sprint and task
     * 
     * @param Sprint $sprint
     * @param integer $date
     * @param EntityManager $entityManager
     * @return SauvegardeSprint | null
     */
    public function saveSprintTasks($sprint, $date, $entityManager) {
        //Check if the sprint has started and if  is not outdated
        $end = $sprint->getDateFin();
        $endDate = date("U", strtotime($end));
        $start = $sprint->getDateDebut();
        $startDate = date("U", strtotime($start));

        if ($startDate <= $date && $date <= $endDate) {
            // Create request from Sprint fields
            $projectCode = $sprint->getProjet()->getCode();
            $version = $sprint->getVersion();
            $request = "project = \"" . $projectCode . "\" AND fixVersion = \"" . $version . "\"";

            // Create connexion and make the request
            $api = new Api("http://agora.villers.pharmagest.com/jira", new Basic('scrochard', 'SeB1;WebC7'));
            $walker = new Walker($api);
            $walker->push($request);

            $originalTime = 0;
            $spentTime = 0;
            $remainedTime = 0;

            // Treat each issue from the request answer
            $entitiesAdded = 0;

            foreach ($walker as $issue) {
                $taskSave = $this->treatTask($issue, $sprint, $entityManager);
                $originalTime += $taskSave->getTpsOriginal();
                $spentTime += $taskSave->getTpsPasse();
                $remainedTime += $taskSave->getTpsRestant();
                $entitiesAdded++;
            }

            // Save a SauvegardeSprint
            return $this->saveSauvegardeSprint($entityManager, $entitiesAdded, $date, $sprint, $originalTime, $spentTime, $remainedTime);
            }
        return;
    }

    /**
     * Save a SauvegardeRelease entity
     * 
     * @param EntityManager $entityManager
     * @param integer $entitiesAdded
     * @param integer $date
     * @param ReleaseProjet $release
     * @param float $originalTime
     * @param float $spentTime
     * @param float $remainedTime
     * @return SauvegardeRelease
     */
    private function saveSauvegardeRelease($entityManager, $entitiesAdded, $date, $release, $originalTime, $spentTime, $remainedTime) {
        // Create entity SauvegardeRelease
        $sauvegardeRelease = new SauvegardeRelease();
        $sauvegardeRelease
                ->setAjout($entitiesAdded)
                ->setDate($date)
                ->setReleaseProjet($release)
                ->setTpsOriginal($originalTime)
                ->setTpsPasse($spentTime)
                ->setTpsRestant($remainedTime);

        //Save entity
        $entityManager->persist($sauvegardeRelease);
        $entityManager->flush();

        return $sauvegardeRelease;
    }

    /**
     * Save a SauvegardeSprint entity
     * 
     * @param EntityManager $entityManager
     * @param integer $entititesAdded
     * @param integer $date
     * @param Sprint $sprint
     * @param float $originalTime
     * @param float $spentTime
     * @param float $remainedTime
     * @return SauvegardeSprint
     */
    private function saveSauvegardeSprint($entityManager, $entititesAdded, $date, $sprint, $originalTime, $spentTime, $remainedTime) {
        // Create entity SauvegardeRelease
        $sauvegardeSprint = new SauvegardeSprint();
        $sauvegardeSprint
                ->setAjout($entititesAdded)
                ->setDate($date)
                ->setSprint($sprint)
                ->setTpsOriginal($originalTime)
                ->setTpsPasse($spentTime)
                ->setTpsRestant($remainedTime);

        //Save entity
        $entityManager->persist($sauvegardeSprint);
        $entityManager->flush();

        return $sauvegardeSprint;
    }

    /**
     * Check if state of a tack is not abandoned
     * 
     * @param Issue $issue
     * @param ReleaseProjet | Sprint $entity
     * @param EntityManager $entityManager
     * @return null | Tache
     */
    private function treatTask($issue, $entity, $entityManager) {

        $key = $issue->getKey();
        $jira = $issue->getFields();

        //If state if abandonned, return null
        if ($jira["Etat"]["name"] == "Abandonné") {
            return null;
        }

        //Today date in good formalism
        $today = date('Y-m-d');
        $date = date("U", strtotime($today));

        //Save the task
        return $this->saveTask($entityManager, $key, $jira, $date, $entity);
    }

    /**
     * 
     * @param EntityManager $entityManager
     * @param key $key
     * @param jira $jira
     * @param integer $date
     * @param ReleaseProjet | Sprint $entity
     * @return \Api\SuiviBundle\Entity\SauvegardeTache
     */
    private function saveTask($entityManager, $key, $jira, $date, $entity) {

        //Get original, spent and remaining time of the task
        $originalTime = intval($jira["Σ Estimation originale"]) / 25200;
        $spentTime = intval($jira["Σ Progrès"]["progress"]) / 25200;
        $remainedTime = intval($jira["Σ Estimation restante"]) / 25200;

        //Check if the task exist en database, if not, create a new one
        $bool = false;
        $tache = $entityManager->getRepository('ApiSuiviBundle:Tache')->findOneBy(array("clef" => $key));
        if (!$tache instanceof Tache) {
            $tache = New Tache();
            $tache
                    ->setClef($key)
                    ->setDescription($jira["Résumé"])
                    ->setPersonne($this->getPerson($jira))
                    ->setIssueType($this->getIssueType($jira));
            $bool = true;
        }

        //Update fields
        $tache->setEtat($jira["Etat"]["name"])
                ->setVAffectee($this->getAffectedVersion($jira));

        //Update or create entity
        $entityManager->persist($tache);
        $entityManager->flush();

        //If new task save resolved versions of the task
        if ($bool) {
            $this->saveResolvedVersions($entityManager, $jira, $tache);
        }

        //Check if the a save for the sauvegardeTache exist with the version of the sprint 'if entity is a sprint
        //Check if sauvegardeTache has been already save today
        if ($entity instanceof Sprint) {
            $version = $entity->getVersion();
            $sauvegardeTache = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')->findOneBy(array("tache" => $tache, "date" => $date, "vResolue" => $version));
        } else {
            $sauvegardeTache = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')->findOneBy(array("tache" => $tache, "date" => $date));
        }

        //If the sauvegardeTache is not save, create a new one
        if (!$sauvegardeTache instanceof SauvegardeTache) {
            $sauvegardeTache = new SauvegardeTache();
            $sauvegardeTache
                    ->setDate($date)
                    ->setEtat($jira["Etat"]["name"])
                    ->setColor($this->getColor($jira))
                    ->setAlerte($this->getAlert($jira))
                    ->setTpsOriginal($originalTime)
                    ->setTpsPasse($spentTime)
                    ->setTpsRestant($remainedTime)
                    ->setVAffectee($this->getAffectedVersion($jira))
                    ->setVResolue($this->getResolvedVersion($jira))
                    ->setTache($tache);

            //Save task in database
            $entityManager->persist($sauvegardeTache);
            $entityManager->flush();
        }

        //If entity is a ReleaseProjet, create a link between task and releaseProjet
        if ($entity instanceof ReleaseProjet) {
            $tacheReleaseProjet = $entityManager->getRepository('ApiSuiviBundle:TacheReleaseProjet')->findOneBy(array("releaseProjet" => $entity, "tache" => $tache));
            if (!$tacheReleaseProjet instanceof TacheReleaseProjet) {
                $tacheReleaseProjet = new TacheReleaseProjet();
                $tacheReleaseProjet
                        ->setReleaseProjet($entity)
                        ->setTache($tache);
                $entityManager->persist($tacheReleaseProjet);
                $entityManager->flush();
            }
        }

        //If entity is a sprint, create a link between task and sprint
        if ($entity instanceof Sprint) {
            $tacheSprint = $entityManager->getRepository('ApiSuiviBundle:TacheSprint')->findOneBy(array("sprint" => $entity, "tache" => $tache));
            if (!$tacheSprint instanceof TacheSprint) {
                $tacheSprint = new TacheSprint();
                $tacheSprint
                        ->setSprint($entity)
                        ->setTache($tache);
                $entityManager->persist($tacheSprint);
                $entityManager->flush();
            }
        }

        return $sauvegardeTache;
    }

    /**
     * Get the alert for a jira
     * 
     * @param jira $jira
     * @return string alert
     */
    private function getAlert($jira) {
        $state = $jira["Etat"]["name"];
        $originalTime = intval($jira["Σ Estimation originale"]) / 25200;
        $remainedTime = intval($jira["Σ Estimation restante"]) / 25200;

        //if remained time and resolved or closed state - alert
        if ($remainedTime > 0 & ($state == "Résolu" || $state == "Clos")) {
            $alert = "images/alert.png";
        }
        //if no remained time and state ongoing - alert
        else if ($remainedTime < 0 & $state == "En cours") {
            $alert = "images/alert.png";
        }
        //if no original time - alert
        else if ($originalTime <= 0) {
            $alert = "images/alert.png";
        }
        //else no alert
        else {
            $alert = "images/check.png";
        }
        return $alert;
    }

    /**
     * Get the color of the jira in term of state
     * 
     * @param jira $jira
     * @return string color
     */
    private function getColor($jira) {
        $state = $jira["Etat"]["name"];
        //if resolved or closed - orange
        if ($state == "Résolu" || $state == "Clos") {
            $color = "moccasin";
        }
        //if ongoing - lightgreen
        else if ($state == "En cours") {
            $color = "lightgreen";
        }
        //if retained - lightblue
        else if ($state == "Retenu") {
            $color = "lightblue";
        }
        //else white
        else {
            $color = "white";
        }
	return $color;
    }

    /**
     * Get version where the jira was affected
     * 
     * @param jira $jira
     * @return string version
     */
    private function getAffectedVersion($jira) {
        if (count($jira["Affecte la/les version(s)"]) > 0) {
            $length = strlen(trim($jira["Affecte la/les version(s)"][0]["name"]));
            if ($length > 0) {
                return $jira["Affecte la/les version(s)"][0]["name"];
            }
        }
        return "&nbsp;";
    }

    /**
     * Get the version where the jira was resolved
     * 
     * @param type $jira
     * @return string
     */
    private function getResolvedVersion($jira) {
        if (count($jira["Version(s) corrigée(s)"]) > 0) {
            $length = strlen(trim($jira["Version(s) corrigée(s)"][0]["name"]));
            if ($length > 0) {
                return $jira["Version(s) corrigée(s)"][0]["name"];
            }
        }
        return "&nbsp;";
    }

    /**
     * Save the version where the jira was resolved
     * 
     * @param EntityManager $entityManager
     * @param jira $jira
     * @param Tache $tache
     */
    private function saveResolvedVersions($entityManager, $jira, $tache) {
        foreach ($jira["Version(s) corrigée(s)"] as $versionCorrige) {
            $name = $versionCorrige["name"];
            $version = new Version();
            $version
                    ->setTache($tache)
                    ->setVersion($name);
            $entityManager->persist($version);
            $entityManager->flush();
        }
    }

    /**
     * Get the type of the jira
     * 
     * @param jira $jira
     * @return string IssueType
     */
    private function getIssueType($jira) {
        return $jira["Type de demande"]["name"];
    }

    /**
     * Get the name of the person affected to the jira
     * 
     * @param type $jira
     * @return type
     */
    private function getPerson($jira) {
        return $jira["Attribution"]["name"];
    }

    /**
     * Delete all the save make today before make a new save
     * 
     * @param integer $date
     * @param EntityManager $entityManager
     */
    public function deleteTodaySave($date, $entityManager) {
        //Erase SauvegardeTache save today
        $sauvegardeTacheSaveToday = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')->findByDate($date);
        foreach ($sauvegardeTacheSaveToday as $sauvegardeTache) {
            $entityManager->remove($sauvegardeTache);
            $entityManager->flush();
        }
        //Erase SauvegardeRelease save today
        $sauvegardeReleaseSaveToday = $entityManager->getRepository('ApiSuiviBundle:SauvegardeRelease')->findByDate($date);
        foreach ($sauvegardeReleaseSaveToday as $releaseSauvegarde) {
            $entityManager->remove($releaseSauvegarde);
            $entityManager->flush();
        }
        //Erase SauvegardeSprint save today
        $sauvegardeSprintSaveToday = $entityManager->getRepository('ApiSuiviBundle:SauvegardeSprint')->findByDate($date);
        foreach ($sauvegardeSprintSaveToday as $sauvegardeSprint) {
            $entityManager->remove($sauvegardeSprint);
            $entityManager->flush();
        }
    }

}
