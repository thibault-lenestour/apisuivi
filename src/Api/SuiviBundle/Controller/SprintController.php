<?php

namespace Api\SuiviBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Api\SuiviBundle\Entity\Sprint;
use Api\SuiviBundle\Form\SprintType;
use Api\SuiviBundle\Controller\SaveCenter;

/**
 * Sprint controller.
 *
 * @Route("/sprint")
 */
class SprintController extends Controller {

    /**
     * Lists all Sprint entities.
     *
     * @Route("/{idRoadmap}/{idProjet}/{boolTermine}", name="sprint")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($idRoadmap, $idProjet, $boolTermine) {
        $entityManager = $this->getDoctrine()->getManager();

        //Get all projet
        $projets = $entityManager->getRepository('ApiSuiviBundle:Projet')->findBy(array(), array("nom" => "ASC"));
        $roadmaps = $entityManager->getRepository('ApiSuiviBundle:Roadmap')->findBy(array(), array("dateDebut" => "DESC"));

        //Get the lastest roadmap by default else the select one
        if ($idRoadmap == 0 && count($roadmaps) > 0) {
            $roadmap = $roadmaps[0];
        } else {
            $roadmap = $entityManager->getRepository('ApiSuiviBundle:Roadmap')->find($idRoadmap);
        }

        //Get all release of all thr project by default else the releases of the select one
        if ($idProjet == 0) {
            $projet = null;
            $releases = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->findBy(array("roadmap" => $roadmap), array("nom" => "ASC"));
        } else {
            $projet = $entityManager->getRepository('ApiSuiviBundle:Projet')->find($idProjet);
            $releases = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->findBy(array("roadmap" => $roadmap, "projet" => $projet), array("nom" => "ASC"));
        }

        //Select sprints in progress if 1, closed if 0, both if 2
        //and all sprints of all project by default else sprints of the select one
        if ($boolTermine == 0 || $boolTermine == 1) {
            if ($idProjet == 0) {
                $projet = null;
                $sprints = $entityManager->getRepository('ApiSuiviBundle:Sprint')->findBy(array("roadmap" => $roadmap, "enCours" => $boolTermine), array("version" => "DESC"));
            } else {
                $projet = $entityManager->getRepository('ApiSuiviBundle:Projet')->find($idProjet);
                $sprints = $entityManager->getRepository('ApiSuiviBundle:Sprint')->findBy(array("roadmap" => $roadmap, "projet" => $projet, "enCours" => $boolTermine), array("version" => "DESC"));
            }
        } else {
            if ($idProjet == 0) {
                $projet = null;
                $sprints = $entityManager->getRepository('ApiSuiviBundle:Sprint')->findBy(array("roadmap" => $roadmap), array("version" => "DESC"));
            } else {
                $projet = $entityManager->getRepository('ApiSuiviBundle:Projet')->find($idProjet);
                $sprints = $entityManager->getRepository('ApiSuiviBundle:Sprint')->findBy(array("roadmap" => $roadmap, "projet" => $projet), array("version" => "DESC"));
            }
        }

        return array(
            'sprints' => $sprints,
            'roadmaps' => $roadmaps,
            'projets' => $projets,
            'roadmapSelected' => $roadmap,
            'projetSelected' => $projet,
            'boolTermine' => $boolTermine
        );
    }

    /**
     * Creates a new Sprint entity.
     *
     * @Route("/", name="sprint_create")
     * @Method("POST")
     * @Template("ApiSuiviBundle:Sprint:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new Sprint();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();
            
            //Get today date on the correct formalism
            $today = date('Y-m-d');
            $date = date("U", strtotime($today));
            
            $saveCenter = new SaveCenter();
            $saveCenter -> saveSprintTasks($entity, $date, $entityManager);

            return $this->redirect($this->generateUrl('sprint', array('idRoadmap' => $entity->getRoadmap()->getId(), 'idProjet' => 0, 'boolTermine' => $entity->getEnCours())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Sprint entity.
     *
     * @param Sprint $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Sprint $entity) {
        $form = $this->createForm(new SprintType(), $entity, array(
            'action' => $this->generateUrl('sprint_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Ajouter ►'));

        return $form;
    }

    /**
     * Displays a form to create a new Sprint entity.
     *
     * @Route("/new", name="sprint_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new Sprint();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Sprint entity.
     *
     * @Route("/{id}", name="sprint_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('ApiSuiviBundle:Sprint')->find($id);

        $sauvegardeSprints = $entityManager->getRepository('ApiSuiviBundle:SauvegardeSprint')->findBy(array("sprint" => $entity), array("date" => "ASC"));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sprint entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        if (count($sauvegardeSprints) > 0) {
            $tpsOriginal = $sauvegardeSprints[0]->getTpsOriginal();
        }

        //get start and end date in the good format
        $dateDebut = $entity->getDateDebut();
        $dateFin = $entity->getDateFin();
        $debut = date("U", strtotime($dateDebut));
        $fin = date("U", strtotime($dateFin)) + 86400;

        //Get number of open day in the sprint (Saturday and Sunday are closed)
        $opendays = 0;
        for ($day = $debut; $day <= $fin; $day = mktime(0, 0, 0, date("m",$day)  , date("d",$day)+1, date("Y",$day))) {
            if (date("l", $day) != "Saturday" && date("l", $day) != "Sunday") {
                $opendays++;
            }
        }

        //Get ratio of the ideal burndown chart
        $rapport = -$tpsOriginal / $opendays;

        //Generate the curve of the ideal burndown chart
        $opendaysSpent = 1;
        $droite = array();
        for ($day = $debut; $day <= $fin; $day = mktime(0, 0, 0, date("m",$day)  , date("d",$day)+1, date("Y",$day))) {
            if (date("l", $day) != "Saturday" && date("l", $day) != "Sunday") {
                $droite[$day] = $tpsOriginal + $rapport * $opendaysSpent;
                $opendaysSpent++;
            }
        }

        return array(
            'entity' => $entity,
            'sauvegardeSprints' => $sauvegardeSprints,
            'tpsOriginal' => $tpsOriginal,
            'droite' => $droite,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Sprint entity.
     *
     * @Route("/{id}/edit", name="sprint_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('ApiSuiviBundle:Sprint')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sprint entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Sprint entity.
     *
     * @param Sprint $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Sprint $entity) {
        $form = $this->createForm(new SprintType(), $entity, array(
            'action' => $this->generateUrl('sprint_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour ►'));

        return $form;
    }

    /**
     * Edits an existing Sprint entity.
     *
     * @Route("/{id}", name="sprint_update")
     * @Method("PUT")
     * @Template("ApiSuiviBundle:Sprint:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('ApiSuiviBundle:Sprint')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sprint entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entityManager->flush();

            return $this->
                            redirect(
                                    $this->generateUrl('sprint', array('idRoadmap' => $entity->getRoadmap()->getId(),
                                        'idProjet' => 0,
                                        'boolTermine' => $entity->getEnCours()
                                            )
                                    )
            );
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Sprint entity.
     *
     * @Route("/{id}", name="sprint_delete")
     * @Method("DELETE")
     */
    public function deleteAction($id) {

        $entityManager = $this->getDoctrine()->getManager();
        $sprint = $entityManager->getRepository('ApiSuiviBundle:Sprint')->find($id);

        //Delete all save of the sprint
        $sauvegardeRelease = $entityManager->getRepository('ApiSuiviBundle:SauvegardeSprint')->findBySprint($sprint);
        foreach ($sauvegardeRelease as $tps) {
            $entityManager->remove($tps);
        }
        //Delete the link between task and sprint
        $tacheReleaseProjets = $entityManager->getRepository('ApiSuiviBundle:TacheSprint')->findBySprint($sprint);
        foreach ($tacheReleaseProjets as $tacheReleaseProjet) {
            $entityManager->remove($tacheReleaseProjet);
        }
        //Delete sprint
        $entityManager->remove($sprint);
        $entityManager->flush();

        return $this->redirect($this->generateUrl('sprint', array('idRoadmap' => 0, 'idProjet' => 0, 'boolTermine' => 2)));
    }

    /**
     * Creates a form to delete a Sprint entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('sprint_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Supprimer ►'))
                        ->getForm()
        ;
    }

    /**
     * Lists of task for a project, a date and a sort column
     *
     * @Route("/tache/{id}/{dateC}/{tri}", name="sprint_tache")
     * @Method("GET")
     * @Template()
     * 
     * @param idSprint $id
     * @param integer $dateC
     * @param string $tri
     * @return list taches
     */
    public function sprintTacheAction($id, $dateC, $tri) {
        $entityManager = $this->getDoctrine()->getManager();

        // Get project
        $sprint = $entityManager->getRepository('ApiSuiviBundle:Sprint')->find($id);

        //Get date
        $date = $this->getDate($entityManager, $dateC, $sprint);

        //Get tasks
        $sauvegardeTache = $this->getTaches($entityManager, $tri, $sprint, $date);

        //Get all time table in descending order
        $sauvegardeSprint = $entityManager
                ->getRepository('ApiSuiviBundle:SauvegardeSprint')
                ->findBy(array("sprint" => $sprint), array('date' => 'DESC'));

        return array(
            'sauvegardeTache' => $sauvegardeTache,
            'sprint' => $sprint,
            'sauvegardeSprint' => $sauvegardeSprint,
            'date' => $date,
            'tri' => $tri
        );
    }

    /**
     * Get last saved task for a project and sort in order $tri
     * 
     * @param EntityManager $entityManager
     * @param String $tri
     * @param ReleaseProjet release
     * @param Date("U) $date
     * @return liste de taches
     */
    private function getTaches($entityManager, $tri, $sprint, $date) {

        //Release tasks
        $tacheSprints = $entityManager->getRepository('ApiSuiviBundle:TacheSprint')->findBySprint($sprint);

        $taches = array();
        //Sort on affected version
        if ($tri == "vaff") {
            foreach ($tacheSprints as $tacheSprint) {
                $tache = $tacheSprint->getTache();
                $dayTache = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')
                        ->findOneBy(array('tache' => $tache, 'date' => $date), array('vAffectee' => 'ASC'));
                array_push($taches, $dayTache);
            }
        }
        //Sort on state
        else if ($tri == "etat") {
            foreach ($tacheSprints as $tacheSprint) {
                $tache = $tacheSprint->getTache();
                $dayTache = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')
                        ->findOneBy(array('tache' => $tache, 'date' => $date), array('etat' => 'ASC'));
                array_push($taches, $dayTache);
            }
        }
        //Sort on key
        else if ($tri == "clef") {
            foreach ($tacheSprints as $tacheSprint) {
                $tache = $tacheSprint->getTache();
                $dayTache = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')
                        ->findOneBy(array('tache' => $tache, 'date' => $date), array('clef' => 'ASC'));
                array_push($taches, $dayTache);
            }
        }
        //Don't sort
        else {
            foreach ($tacheSprints as $tacheSprint) {
                $tache = $tacheSprint->getTache();
                $dayTache = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')
                        ->findOneBy(array('tache' => $tache, 'date' => $date));
                array_push($taches, $dayTache);
            }
        }

        return $taches;
    }

    /**
     * Get the choosen date or the last saved date if no choosen date
     * 
     * @param EntityManager $entityManager
     * @param integer $dateC
     * @param ReleaseProjet $release
     * @return integer date
     */
    private function getDate($entityManager, $dateC, $sprint) {
        if ($dateC == 0) {
            // Get save date in descending order
            $repository = $entityManager->getRepository('ApiSuiviBundle:SauvegardeSprint');
            $query = $repository
                    ->createQueryBuilder('tps')
                    ->select('tps.date')
                    ->distinct()
                    ->where('tps.sprint = :sprint')
                    ->setParameter('sprint', $sprint)
                    ->orderBy('tps.id', 'DESC') 
                    ->getQuery();
            $dates = $query->getResult();
            if (count($dates) > 0) {
                return $dates[0]["date"];
            } else {
                return 0;
            }
        } else {
            return $dateC;
        }
    }

}
