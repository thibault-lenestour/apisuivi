<?php

namespace Api\SuiviBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Api\SuiviBundle\Entity\Projet;
use Api\SuiviBundle\Form\ProjetType;

/**
 * Projet controller.
 *
 * @Route("/projet")
 */
class ProjetController extends Controller {

    /**
     * Lists all Projet entities.
     *
     * @Route("/", name="projet")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $entityManager = $this->getDoctrine()->getManager();

        //get in alphabetic order
        $entities = $entityManager->getRepository('ApiSuiviBundle:Projet')->findBy(array(), array("nom" => "ASC"));

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Projet entity.
     *
     * @Route("/", name="projet_create")
     * @Method("POST")
     * @Template("ApiSuiviBundle:Projet:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new Projet();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('projet'));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Projet entity.
     *
     * @param Projet $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Projet $entity) {
        $form = $this->createForm(new ProjetType(), $entity, array(
            'action' => $this->generateUrl('projet_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Ajouter ►'));

        return $form;
    }

    /**
     * Displays a form to create a new Projet entity.
     *
     * @Route("/new", name="projet_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new Projet();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Projet entity.
     *
     * @Route("/{id}", name="projet_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $entityManager = $this->getDoctrine()->getManager();

        $projet = $entityManager->getRepository('ApiSuiviBundle:Projet')->find($id);
        $sprints = $entityManager->getRepository('ApiSuiviBundle:Sprint')->findBy(array("projet" => $projet), array("version" => "DESC"));
        $releases = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->findBy(array("projet" => $projet), array("nom" => "ASC"));

        if (!$projet) {
            throw $this->createNotFoundException('Unable to find Projet entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'projet' => $projet,
            'sprints' => $sprints,
            'releases' => $releases,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Projet entity.
     *
     * @Route("/{id}/edit", name="projet_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('ApiSuiviBundle:Projet')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Projet entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Projet entity.
     *
     * @param Projet $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Projet $entity) {
        $form = $this->createForm(new ProjetType(), $entity, array(
            'action' => $this->generateUrl('projet_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour ►'));

        return $form;
    }

    /**
     * Edits an existing Projet entity.
     *
     * @Route("/{id}", name="projet_update")
     * @Method("PUT")
     * @Template("ApiSuiviBundle:Projet:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('ApiSuiviBundle:Projet')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Projet entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entityManager->flush();

            return $this->redirect($this->generateUrl('projet'));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Projet entity.
     *
     * @Route("/{id}", name="projet_delete")
     */
    public function deleteAction($id) {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('ApiSuiviBundle:Projet')->find($id);
        $sprints = $entityManager->getRepository('ApiSuiviBundle:Sprint')->findByProjet($entity);
        $releases = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->findByProjet($entity);

        //Permit to delete the roadmap if no link to sprint or to release
        if (count($sprints) <= 0 && count($releases) <= 0) {
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Projet entity.');
            }
            $entityManager->remove($entity);
            $entityManager->flush();
            return $this->redirect($this->generateUrl('projet'));
        } else {
            $entities = $entityManager->getRepository('ApiSuiviBundle:Projet')->findAll();
            return $this->
                            render(
                                    'ApiSuiviBundle:Projet:index.html.twig', array(
                                'entities' => $entities,
                                'error' => "error"
                                    )
            );
        }
    }

    /**
     * Creates a form to delete a Projet entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('projet_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Supprimer ►'))
                        ->getForm()
        ;
    }

}
