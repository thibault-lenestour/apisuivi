<?php

namespace Api\SuiviBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Api\SuiviBundle\Entity\Log;
use Api\SuiviBundle\Controller\SaveCenter;

/**
 * Log controller.
 *
 * @Route("/log")
 */
class LogController extends Controller {

    /**
     * Lists all Log entities.
     *
     * @Route("/", name="log")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $entityManager = $this->getDoctrine()->getManager();

        //Get from the lastest to the oldest
        $entities = $entityManager->getRepository('ApiSuiviBundle:Log')->findBy(array(), array("date" => "DESC"));

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Deletes a Log entity.
     *
     * @Route("/{nb}", name="log_delete")
     */
    public function deleteAction($nb) {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $entityManager->getRepository('ApiSuiviBundle:Log');

        $query = $repository->createQueryBuilder('log')
                ->orderBy('log.date', 'DESC')
                ->setFirstResult($nb)
                ->getQuery();

        $logs = $query->getResult();

        foreach ($logs as $log) {
            $entityManager->remove($log);
            $entityManager->flush();
        }

        return $this->redirect($this->generateUrl('log'));
    }

    /**
     * Save data in database if this is no save today
     * 
     * @return type
     */
    public function saveAction() {

        $entityManager = $this->getDoctrine()->getManager();

        //Get all project
        $releases = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->findAll();

        //Get today date on the correct formalism
        $today = date('Y-m-d');
        $date = date("U", strtotime($today));

        $saveCenter = new SaveCenter();
        $saveCenter->deleteTodaySave($date, $entityManager);

        $saveInfoRelease = array();
        foreach ($releases as $release) {
            array_push($saveInfoRelease, $saveCenter->saveReleaseTasks($release, $date, $entityManager));
        }

        $sprints = $entityManager->getRepository('ApiSuiviBundle:Sprint')->findAll();
        $saveInfoSprint = array();
        foreach ($sprints as $sprint) {
            array_push($saveInfoSprint, $saveCenter->saveSprintTasks($sprint, $date, $entityManager));
        }

        //Create a log (it will be display in the table)
        $log = new Log();
        $log->setDate(date("U"))
                ->setTrace(
                        $this->convertSaveInfoReleaseToTrace($saveInfoRelease)
                        . "<br>"
                        . $this->convertSaveInfoSprintToTrace($saveInfoSprint)
                )
                ->setTypeSave("Manuelle");
        $entityManager->persist($log);
        $entityManager->flush();

        return $this->redirect($this->generateUrl('log'));
    }

    /**
     * Convert array with release save information into string
     * 
     * @param array $saveInfoRelease
     * @return string trace
     */
    private function convertSaveInfoReleaseToTrace($saveInfoRelease) {
        $trace = "";
        //Check if the array is not empty
        if (count($saveInfoRelease) > 0) {
            //Each info in the array
            foreach ($saveInfoRelease as $tab) {
                //Check if the info is not null
                if($tab != null){
                    $trace .=
                            $tab->getAjout() .
                            " Jira ajoutées à la base de données pour la release " .
                            $tab->getReleaseProjet()->getNom() . "</br>";
                }
            }
        } else {
            $trace .= "La base de données est à jour";
        }
        return $trace;
    }

    /**
     * Convert array with sprint save information into string
     * 
     * @param type $saveInfoSprint
     * @return string
     */
    private function convertSaveInfoSprintToTrace($saveInfoSprint) {
        $trace = "";
        //Check if the array is not empty
        if (count($saveInfoSprint) > 0) {
            //Each info in the array
            foreach ($saveInfoSprint as $tab) {
                //Check if the info is not null
                if($tab != null){
                    $trace .=
                            $tab->getAjout() .
                            " Jira reliées à la base de données pour le sprint concernant la version " .
                            $tab->getSprint()->getVersion() . " du projet " . $tab->getSprint()->getProjet()->getNom() . "</br>";
                }
            }
        } else {
            $trace .= "La base de données est à jour";
        }
        return $trace;
    }

}
