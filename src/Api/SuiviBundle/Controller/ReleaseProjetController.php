<?php

namespace Api\SuiviBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Api\SuiviBundle\Entity\ReleaseProjet;
use Api\SuiviBundle\Form\ReleaseProjetType;
use Api\SuiviBundle\Controller\SaveCenter;

/**
 * Release controller.
 *
 * @Route("/release")
 */
class ReleaseProjetController extends Controller {

    /**
     * Lists all Release entities.
     *
     * @Route("/{idRoadmap}/{idProjet}", name="release")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($idRoadmap, $idProjet) {
        $entityManager = $this->getDoctrine()->getManager();

        //Get list of roadmap and list of projects
        $roadmaps = $entityManager->getRepository('ApiSuiviBundle:Roadmap')->findBy(array(), array("dateDebut" => "DESC"));
        $projets = $entityManager->getRepository('ApiSuiviBundle:Projet')->findBy(array(), array("nom" => "ASC"));

        //Get the lastest roadmap by default else the select one
        if ($idRoadmap == 0 && count($roadmaps) > 0) {
            $roadmap = $roadmaps[0];
        } else {
            $roadmap = $entityManager->getRepository('ApiSuiviBundle:Roadmap')->find($idRoadmap);
        }

        //Get all release of all project by default else the releases of the select one
        if ($idProjet == 0) {
            $projet = null;
            $releases = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->findBy(array("roadmap" => $roadmap), array("nom" => "ASC"));
        } else {
            $projet = $entityManager->getRepository('ApiSuiviBundle:Projet')->find($idProjet);
            $releases = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->findBy(array("roadmap" => $roadmap, "projet" => $projet), array("nom" => "ASC"));
        }

        return array(
            'releases' => $releases,
            'roadmaps' => $roadmaps,
            'projets' => $projets,
            'roadmapSelected' => $roadmap,
            'projetSelected' => $projet
        );
    }

    /**
     * Creates a new Release entity.
     *
     * @Route("/", name="release_create")
     * @Method("POST")
     * @Template("ApiSuiviBundle:ReleaseProjet:new.html.twig")
     */
    public function createAction(Request $request) {
        $release = new ReleaseProjet();
        $form = $this->createCreateForm($release);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($release);
            $entityManager->flush();
            
            //Get today date on the correct formalism
            $today = date('Y-m-d');
            $date = date("U", strtotime($today));
            
            $saveCenter = new SaveCenter();
            $saveCenter -> saveReleaseTasks($release, $date, $entityManager);

            return $this->
                            redirect(
                                    $this->generateUrl('release', array('idRoadmap' => $release->getRoadmap()->getId(),
                                        'idProjet' => 0
                                            )
                                    )
            );
        }

        return array(
            'entity' => $release,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Release entity.
     *
     * @param ReleaseProjet $release The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ReleaseProjet $release) {
        $form = $this->createForm(new ReleaseProjetType(), $release, array(
            'action' => $this->generateUrl('release_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Ajouter ►'));

        return $form;
    }

    /**
     * Displays a form to create a new Release entity.
     *
     * @Route("/new", name="release_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $release = new ReleaseProjet();
        $form = $this->createCreateForm($release);

        return array(
            'release' => $release,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Release entity.
     *
     * @Route("/{id}", name="release_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $entityManager = $this->getDoctrine()->getManager();

        $release = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->find($id);
        //Get the lastest save
        $sauvegardeRelease = $entityManager
                ->getRepository('ApiSuiviBundle:SauvegardeRelease')
                ->findOneBy(array('releaseProjet' => $release), array('date' => 'DESC'));

        if (!$release) {
            throw $this->createNotFoundException('Unable to find Release entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'release' => $release,
            'delete_form' => $deleteForm->createView(),
            'sauvegardeRelease' => $sauvegardeRelease
        );
    }

    /**
     * Displays a form to edit an existing Release entity.
     *
     * @Route("/{id}/edit", name="release_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $entityManager = $this->getDoctrine()->getManager();

        $release = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->find($id);

        if (!$release) {
            throw $this->createNotFoundException('Unable to find Release entity.');
        }

        $editForm = $this->createEditForm($release);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'release' => $release,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Release entity.
     *
     * @param ReleaseProjet $release The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ReleaseProjet $release) {
        $form = $this->createForm(new ReleaseProjetType(), $release, array(
            'action' => $this->generateUrl('release_update', array('id' => $release->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour ►'));

        return $form;
    }

    /**
     * Edits an existing Release entity.
     *
     * @Route("/{id}", name="release_update")
     * @Method("PUT")
     * @Template("ApiSuiviBundle:Release:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $entityManager = $this->getDoctrine()->getManager();

        $release = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->find($id);

        if (!$release) {
            throw $this->createNotFoundException('Unable to find Release entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($release);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entityManager->flush();

            return $this->
                            redirect(
                                    $this->generateUrl('release', array('idRoadmap' => $release->getRoadmap()->getId(),
                                        'idProjet' => 0
                                            )
                                    )
            );
        }

        return array(
            'release' => $release,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Release entity.
     *
     * @Route("/{id}", name="release_delete")
     */
    public function deleteAction($id) {
        $entityManager = $this->getDoctrine()->getManager();
        $release = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->find($id);

        //Delete all save of the release
        $sauvegardeRelease = $entityManager->getRepository('ApiSuiviBundle:SauvegardeRelease')->findByReleaseProjet($release);
        foreach ($sauvegardeRelease as $tps) {
            $entityManager->remove($tps);
        }
        //Delete the link between task and release
        $tacheReleaseProjets = $entityManager->getRepository('ApiSuiviBundle:TacheReleaseProjet')->findByReleaseProjet($release);
        foreach ($tacheReleaseProjets as $tacheReleaseProjet) {
            $entityManager->remove($tacheReleaseProjet);
        }
        //Delete release
        $entityManager->remove($release);
        $entityManager->flush();

        return $this->redirect($this->generateUrl('release', array('idRoadmap' => 0, 'idProjet' => 0)));
    }

    /**
     * Creates a form to delete a Release entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('release_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Supprimer ►'))
                        ->getForm()
        ;
    }

    /**
     * Display graphic of project historic
     * 
     * @param ReleaseId $id
     * @return type
     */
    public function graphAction($id) {

        $entityManager = $this->getDoctrine()->getManager();

        //Get the project
        $releaseProjet = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->find($id);

        //Get time entity (time with same project - different date)
        $sauvegardeRelease = $entityManager->getRepository('ApiSuiviBundle:SauvegardeRelease')
                ->findBy(array("releaseProjet" => $releaseProjet), array('date' => 'ASC'));

        //Get original time
        if (count($sauvegardeRelease) > 0) {
            $tempsOriginal = $sauvegardeRelease[0]->getTpsOriginal();
        } else {
            $tempsOriginal = 0;
        }

        return $this->render(
                        'ApiSuiviBundle:ReleaseProjet:graph.html.twig', array(
                    'releaseProjet' => $releaseProjet,
                    'sauvegardeRelease' => $sauvegardeRelease,
                    'tempsOriginal' => $tempsOriginal
                        )
        );
    }

    /**
     * Lists of task for a project, a date and a sort column
     *
     * @Route("/tache/{id}/{dateC}/{tri}", name="release_tache")
     * @Method("GET")
     * @Template()
     * 
     * @param idRelease $id
     * @param integer $dateC
     * @param string $tri
     * @return list taches
     */
    public function releaseTacheAction($id, $dateC, $tri) {
        $entityManager = $this->getDoctrine()->getManager();

        // Get project
        $releaseProjet = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->find($id);

        //Get date
        $date = $this->getDate($entityManager, $dateC, $releaseProjet);

        //Get tasks
        $sauvegardeTache = $this->getTaches($entityManager, $tri, $releaseProjet, $date);

        //Get all time table in descending order
        $sauvegardeRelease = $entityManager
                ->getRepository('ApiSuiviBundle:SauvegardeRelease')
                ->findBy(array("releaseProjet" => $releaseProjet), array('date' => 'DESC'));

        return array(
            'sauvegardeTache' => $sauvegardeTache,
            'releaseProjet' => $releaseProjet,
            'sauvegardeRelease' => $sauvegardeRelease,
            'date' => $date,
            'tri' => $tri
        );
    }

    /**
     * Get last saved task for a project and sort in order $tri
     * 
     * @param EntityManager $entityManager
     * @param String $tri
     * @param ReleaseProjet release
     * @param Date("U) $date
     * @return liste de taches
     */
    private function getTaches($entityManager, $tri, $releaseProjet, $date) {

        //Release tasks
        $tacheReleaseProjets = $entityManager->getRepository('ApiSuiviBundle:TacheReleaseProjet')->findByReleaseProjet($releaseProjet);

        $taches = array();
        //Sort on affected version
        if ($tri == "vaff") {
            foreach ($tacheReleaseProjets as $tacheReleaseProjet) {
                $tache = $tacheReleaseProjet->getTache();
                $dayTache = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')
                        ->findOneBy(array('tache' => $tache, 'date' => $date), array('vAffectee' => 'ASC'));
                array_push($taches, $dayTache);
            }
        }
        //Sort on resolved version
        else if ($tri == "vres") {
            foreach ($tacheReleaseProjets as $tacheReleaseProjet) {
                $tache = $tacheReleaseProjet->getTache();
                $dayTache = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')
                        ->findOneBy(array('tache' => $tache, 'date' => $date), array('vResolue' => 'ASC'));
                array_push($taches, $dayTache);
            }
        }
        //Sort on state
        else if ($tri == "etat") {
            foreach ($tacheReleaseProjets as $tacheReleaseProjet) {
                $tache = $tacheReleaseProjet->getTache();
                $dayTache = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')
                        ->findOneBy(array('tache' => $tache, 'date' => $date), array('etat' => 'ASC'));
                array_push($taches, $dayTache);
            }
        }
        //Sort on key
        else if ($tri == "clef") {
            foreach ($tacheReleaseProjets as $tacheReleaseProjet) {
                $tache = $tacheReleaseProjet->getTache();
                $dayTache = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')
                        ->findOneBy(array('tache' => $tache, 'date' => $date), array('clef' => 'ASC'));
                array_push($taches, $dayTache);
            }
        }
        //Don't sort
        else {
            foreach ($tacheReleaseProjets as $tacheReleaseProjet) {
                $tache = $tacheReleaseProjet->getTache();
                $dayTache = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')
                        ->findOneBy(array('tache' => $tache, 'date' => $date));
                array_push($taches, $dayTache);
            }
        }

        return $taches;
    }

    /**
     * Get the choosen date or the last saved date if no choosen date
     * 
     * @param EntityManager $entityManager
     * @param integer $dateC
     * @param ReleaseProjet $release
     * @return integer date
     */
    private function getDate($entityManager, $dateC, $releaseProjet) {
        if ($dateC == 0) {
            // Get save date in descending order
            $repository = $entityManager->getRepository('ApiSuiviBundle:SauvegardeRelease');
            $query = $repository
                    ->createQueryBuilder('tps')
                    ->select('tps.date')
                    ->distinct()
                    ->where('tps.releaseProjet = :releaseProjet')
                    ->setParameter('releaseProjet', $releaseProjet)
                    ->orderBy('tps.id', 'DESC')
                    ->getQuery();
            $dates = $query->getResult();
            if (count($dates) > 0) {
                return $dates[0]["date"];
            } else {
                return 0;
            }
        } else {
            return $dateC;
        }
    }

}
