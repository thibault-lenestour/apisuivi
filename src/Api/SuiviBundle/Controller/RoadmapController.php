<?php

namespace Api\SuiviBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Api\SuiviBundle\Entity\Roadmap;
use Api\SuiviBundle\Form\RoadmapType;

/**
 * Roadmap controller.
 *
 * @Route("/roadmap")
 */
class RoadmapController extends Controller {

    /**
     * Lists all Roadmap entities.
     *
     * @Route("/", name="roadmap")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $entityManager = $this->getDoctrine()->getManager();

        $entities = $entityManager->getRepository('ApiSuiviBundle:Roadmap')->findBy(array(), array("dateDebut" => "DESC"));

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Roadmap entity.
     *
     * @Route("/", name="roadmap_create")
     * @Method("POST")
     * @Template("ApiSuiviBundle:Roadmap:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new Roadmap();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('roadmap'));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Roadmap entity.
     *
     * @param Roadmap $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Roadmap $entity) {
        $form = $this->createForm(new RoadmapType(), $entity, array(
            'action' => $this->generateUrl('roadmap_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Ajouter ►'));

        return $form;
    }

    /**
     * Displays a form to create a new Roadmap entity.
     *
     * @Route("/new", name="roadmap_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new Roadmap();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Roadmap entity.
     *
     * @Route("/{id}", name="roadmap_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $entityManager = $this->getDoctrine()->getManager();

        $roadmap = $entityManager->getRepository('ApiSuiviBundle:Roadmap')->find($id);
        $sprints = $entityManager->getRepository('ApiSuiviBundle:Sprint')->findBy(array("roadmap" => $roadmap), array("projet" => "ASC"));
        $releases = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->findBy(array("roadmap" => $roadmap), array("nom" => "ASC"));

        if (!$roadmap) {
            throw $this->createNotFoundException('Unable to find Roadmap entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'roadmap' => $roadmap,
            'sprints' => $sprints,
            'releases' => $releases,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Roadmap entity.
     *
     * @Route("/{id}/edit", name="roadmap_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('ApiSuiviBundle:Roadmap')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Roadmap entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Roadmap entity.
     *
     * @param Roadmap $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Roadmap $entity) {
        $form = $this->createForm(new RoadmapType(), $entity, array(
            'action' => $this->generateUrl('roadmap_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour ►'));

        return $form;
    }

    /**
     * Edits an existing Roadmap entity.
     *
     * @Route("/{id}", name="roadmap_update")
     * @Method("PUT")
     * @Template("ApiSuiviBundle:Roadmap:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('ApiSuiviBundle:Roadmap')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Roadmap entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $entityManager->flush();
            return $this->redirect($this->generateUrl('roadmap'));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Roadmap entity.
     *
     * @Route("/{id}", name="roadmap_delete")
     */
    public function deleteAction($id) {
        $entityManager = $this->getDoctrine()->getManager();

        $entity = $entityManager->getRepository('ApiSuiviBundle:Roadmap')->find($id);
        $sprints = $entityManager->getRepository('ApiSuiviBundle:Sprint')->findByRoadmap($entity);
        $releases = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->findByRoadmap($entity);

        //Permit to delete the roadmap if no link to sprint or to release
        if (count($sprints) <= 0 && count($releases) <= 0) {
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Roadmap entity.');
            }
            $entityManager->remove($entity);
            $entityManager->flush();
            return $this->redirect($this->generateUrl('roadmap'));
        } else {
            $entities = $entityManager->getRepository('ApiSuiviBundle:Roadmap')->findAll();
            return $this->
                            render(
                                    'ApiSuiviBundle:Roadmap:index.html.twig', array(
                                'entities' => $entities,
                                'error' => "error"
                                    )
            );
        }
    }

    /**
     * Creates a form to delete a Roadmap entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('roadmap_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Supprimer ►'))
                        ->getForm()
        ;
    }

}
