<?php

namespace Api\SuiviBundle\Controller\Jira\Api\Authentication;

interface AuthenticationInterface {

    public function getCredential();

    public function getId();

    public function getPassword();
}
