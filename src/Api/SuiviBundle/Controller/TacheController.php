<?php

namespace Api\SuiviBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Tache controller.
 *
 * @Route("/tache")
 */
class TacheController extends Controller {

    /**
     * Lists all Tache entities.
     *
     * @Route("/", name="tache")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $entityManager = $this->getDoctrine()->getManager();

        $entities = $entityManager->getRepository('ApiSuiviBundle:Tache')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Find task for a given release.
     *
     * @Route("/showRelease/{id}", name="tache_show_release")
     * @Method("GET")
     * @Template()
     */
    public function showReleaseAction($id) {
        $entityManager = $this->getDoctrine()->getManager();

        $tacheSauvegardee = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')->find($id);
        $tacheReleaseProjet = $entityManager->getRepository('ApiSuiviBundle:TacheReleaseProjet')->findOneByTache($tacheSauvegardee->getTache());
        $entities = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')->findBy(array('tache' => $tacheReleaseProjet->getTache()), array('date' => 'ASC'));

        return array(
            'entities' => $entities,
            'tacheSauvegardee' => $tacheSauvegardee,
            'tacheReleaseProjet' => $tacheReleaseProjet,
        );
    }

    /**
     * Find tasks for a given sprint.
     *
     * @Route("/showSprint/{id}", name="tache_show_sprint")
     * @Method("GET")
     * @Template()
     */
    public function showSprintAction($id) {
        $entityManager = $this->getDoctrine()->getManager();

        $tacheSauvegardee = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')->find($id);
        $tacheSprint = $entityManager->getRepository('ApiSuiviBundle:TacheSprint')->findOneByTache($tacheSauvegardee->getTache());
        $entities = $entityManager->getRepository('ApiSuiviBundle:SauvegardeTache')->findBy(array('tache' => $tacheSprint->getTache()), array('date' => 'ASC'));

        return array(
            'entities' => $entities,
            'tacheSauvegardee' => $tacheSauvegardee,
            'tacheSprint' => $tacheSprint,
        );
    }

}
