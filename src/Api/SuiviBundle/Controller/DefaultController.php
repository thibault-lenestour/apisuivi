<?php

namespace Api\SuiviBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

use Api\SuiviBundle\Controller\Jira\Api;
use Api\SuiviBundle\Controller\Jira\Api\Authentication\Basic;
use Api\SuiviBundle\Controller\Jira\Issues\Walker;

class DefaultController extends Controller {
    
    /**
     * 
     */
    public function testAction(){
        //Get request
        $request = "project = WDIR AND issuetype in (Bug, Improvement) AND affectedVersion in (13660, 13762, 13763) AND component in ('PFP 554 (V2)' )";

        // Create connexion and make the request
        $api = new Api("http://agora.villers.pharmagest.com/jira", new Basic('scrochard', 'SeB1;WebC7'));
        $walker = new Walker($api);
        $walker->push($request);

        $i = 0;
        foreach ($walker as $issue) {
            $jira = $issue->getFields();
            if(count($jira["Sous-tâches"])>0){
                foreach($jira["Sous-tâches"] as $sousTacheJira){
                    $key = $sousTacheJira["key"];
                    $r = "project = WDIR AND key = ".$key;
                    $apiR = new Api("http://agora.villers.pharmagest.com/jira", new Basic('scrochard', 'SeB1;WebC7'));
                    $walkerR = new Walker($apiR);
                    $walkerR->push($r);
                    
                    foreach ($walkerR as $issueR) {
                        $sousTache =$issueR->getFields();
                        
                        echo "Personne : " . $sousTache["Attribution"]["name"] . "<br>";
                        echo "Type : Sous Tache <br>";
                        echo "Original : " . intval($sousTache["Σ Estimation originale"]) / 25200 . "<br>";
                        echo "Passé : " . intval($sousTache["Σ Progrès"]["progress"]) / 25200 . "<br>";
                        echo "Restant : " . intval($sousTache["Σ Estimation restante"]) / 25200 . "<br>";
                    }
                   
                    echo "<br>";
                    
                    $i++;
                }
            }else{
                echo "Personne : " . $jira["Attribution"]["name"] . "<br>";
                echo "Type : Tache <br>";
                echo "Original : " . intval($jira["Σ Estimation originale"]) / 25200 . "<br>";
                echo "Passé : " . intval($jira["Σ Progrès"]["progress"]) / 25200 . "<br>";
                echo "Restant : " . intval($jira["Σ Estimation restante"]) / 25200 . "<br>";
                echo "<br>";
            }
        }

        return $this->render('ApiSuiviBundle:Default:test.html.twig');
    }

    /**
     * List all project to set on home page
     * 
     * @return view
     */
    public function accueilAction() {

        $request = $this->getRequest();
        $session = $request->getSession();

        $entityManager = $this->getDoctrine()->getManager();

        //Get all project in database
        $releases = $entityManager->getRepository('ApiSuiviBundle:ReleaseProjet')->findAll();

        $listSauvegardeRelease = array();
        // Get time table for each project
        foreach ($releases as $releaseProjet) {
            //Get time save during last data updated
            $sauvegardeRelease = $entityManager
                    ->getRepository('ApiSuiviBundle:SauvegardeRelease')
                    ->findBy(
                    array('releaseProjet' => $releaseProjet), array('date' => 'DESC'), 
                    1,
                    0
            );

            //If have a result, link time torelease in an array
            if (count($sauvegardeRelease) > 0) {
                $listSauvegardeRelease[$releaseProjet->getId()] = $sauvegardeRelease[0];
            } else {
                $listSauvegardeRelease[$releaseProjet->getId()] = null;
            }
        }

        // Get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->
                        render(
                                'ApiSuiviBundle:Default:accueil.html.twig',
                                array(
                            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                            'error' => $error,
                            'releases' => $releases,
                            'listSauvegardeRelease' => $listSauvegardeRelease
                                )
        );
    }

}
