<?php

namespace Api\SuiviBundle\Tests\Controller;

use PHPUnit_Extensions_Selenium2TestCase;

require_once 'src/Api/SuiviBundle/Controller/DefaultController.php';

class WebTest extends PHPUnit_Extensions_Selenium2TestCase{
    
    protected function setUp()
    {
        $this->setBrowser('firefox');
        $this->setBrowserUrl($this->getBaseUrl());
    }
    
    private function getBaseUrl(){
        return 'http://101.0.0.251/web/app_dev.php';
    }
    
    private function setUrl($url){
        $this->url($this->getBaseUrl().$url);
    }
    
    private function setHomepage(){
        $this->setUrl('/');
    }
    
    
    private function connectTo($login, $mdp, $url){
        $this->setHomepage();
        $this -> byId("connexion_identifiant") -> value($login);
        $this -> byId("connexion_mdp") -> value($mdp);
        $this -> byName( 'connexion' ) -> submit();
        
        $this -> setUrl($url);
    }

    public function testTitle()
    {
        $this->setHomepage();
        $this->assertEquals('Roadmaps Tool - Accueil', $this->title());
    }
    
    /**
     * Scenario: Fail Admin Connexion
     */
    public function testFailedConnexion(){
        #Given I am on homepage
        $this->setHomepage();
        
        #When I fill in "_username" with "administrateur"
        $identifiant = $this -> byId("connexion_identifiant");
        $identifiant -> value("administrateur");
        
        #And I fill in "_password" with "admin"
        $mdp = $this -> byId("connexion_mdp");
        $mdp -> value("admin");
        
        #And I press "login"
        $form = $this->byName( 'connexion' ); 
        $form -> submit();
        
        #Then I should see the modal "Erreur identifiant/mot de passe"
        $this ->assertEquals("Erreur identifiant/mot de passe", $this -> alertText());
        
    }

    /**
     * Admin Connexion
     * @covers DefaultController::accueilAction
     */
    public function testConnexion(){
        #Given I am on homepage
        $this->setHomepage();
        
        #When I fill in "nl_identifiant" with "administrateur"
        $identifiant = $this -> byId("connexion_identifiant");
        $identifiant -> value("administrateur");
        #And I fill in "nl_mdp" with "R0admap$"
        $mdp = $this -> byId("connexion_mdp");
        $mdp -> value("R0admap$");
        #And I press "login"
        //$button = $this -> byName("login");
        //$button -> click();
        $form = $this->byName( 'connexion' ); 
        $form -> submit();
        
        #Then I should see "Compte"
        $element = $this->byCssSelector('nav')->text();
        $this->assertContains("COMPTE", $element);
        
        #When I press "Se déconnecter"
        $button = $this -> byName("logout");
        $button ->click();
        
        #Then I should not see "Compte"
        $element = $this->byCssSelector('nav')->text();
        $this->assertNotContains("COMPTE", $element);
    }
    
    /**
     * Scenario: Go to projects list and see it
     */
    public function testGoToProject(){
        #Given I am on homepage
        $this->setHomepage();
        #When I follow "projet"
        $link = $this->byLinkText("PROJET");
        $link -> click();
        #Then I should be on "/projet/"
        $currentUrl = $this -> url();
        $this -> assertEquals($this->getBaseUrl()."/projet/",$currentUrl);
        #And I should see "Liste des projets"
        //$success = $this->byId("container")->byCssSelector('h1')->text();
        //$this-> assertEquals( "Liste des projets", $success );
        $this -> assertNotNull($this ->byXPath("//h1[contains(.,'Liste des projets')]"));
        #And I should see an "table" element
        $this -> assertNotNull($this -> byClassName("records_list"));
    }
    
    /**
     * Scenario: Create a project
     */
    public function testCreateProject(){
        #Given I am connected with "administrateur" and "R0admap$" on "/projet/"
        $this ->connectTo("administrateur", "R0admap$", "/projet/");
        
        #When I press "api_suivibundle_add_projet"
        $button = $this -> byId("api_suivibundle_add_projet");
        $button ->click();
        
        #Then I should see an "form" element
        $this -> assertNotNull($this ->byXPath("//form[starts-with(@name, 'api_suivibundle_projet')]"));
        
        #When I fill in "api_suivibundle_projet_nom" with "Projet Test"
        $nom = $this -> byId("api_suivibundle_projet_nom");
        $nom -> value("Projet Test");
        
        #And I fill in "api_suivibundle_projet_description" with "Ceci est un projet de test"
        $description = $this -> byId("api_suivibundle_projet_description");
        $description -> value("Ceci est un projet de test");
        
        #And I fill in "api_suivibundle_projet_code" with "TEST"
        $code = $this -> byId("api_suivibundle_projet_code");
        $code -> value("TEST");
        
        #And I press "api_suivibundle_projet_submit"
        $form = $this->byId( 'api_suivibundle_projet_submit' ); 
        $form -> submit();
        
        #Then I should be on "/projet/"
        $currentUrl = $this -> url();
        $this -> assertEquals($this->getBaseUrl()."/projet/",$currentUrl);
        
        #And I should see "Projet Test"
        $this -> assertNotNull($this ->byXPath("//td[contains(.,'Projet Test')]"));
        
        #And I clean projet creation "Projet Test"
        $button = $this -> byId("api_suivibundle_delete_projet_TEST");
        $button ->click();
        $this ->acceptAlert();
        
        $element = $this->byCssSelector('td')->text();
        $this->assertNotContains("Projet Test", $element);
    }
    
    /**
     * Consult a task historic
     */
    public function testNavigationToTask(){
        #Given I am on homepage
        $this->setHomepage();
        #When I press "historique_Offiref"
        $button = $this -> byId("historique_Offiref");
        $button ->click();
        #Then I should see "Historique du release Offiref"
        $this -> assertNotNull($this ->byXPath("//h1[contains(.,'Historique du release Offiref')]"));
        #And I should see a div named "containerGraph"
        $this -> assertNotNull($this ->byXPath("//div[starts-with(@id, 'containerGraph')]"));
        #When I press "Histogramme"
        $button = $this ->byXPath("//button[contains(.,'Histogramme')]");
        $button ->click();
        #Then I should see "Release Offiref"
        $this -> assertNotNull($this ->byXPath("//h1[contains(.,'Release Offiref')]"));
        #And I should see a div named "chart_div"
        $this -> assertNotNull($this ->byXPath("//div[starts-with(@id, 'chart_div')]"));
        #When I press "Liste des taches"
        $button = $this ->byXPath("//button[contains(.,'Liste des taches')]");
        $button ->click();
        #Then I should see an "table" element
        $this -> assertNotNull($this -> byClassName("records_list"));
        #And I should see "WREF-38"
        $this -> assertNotNull($this ->byXPath("//td[contains(.,'WREF-38')]"));
        #When I follow "tache_WREF-38"
        $button = $this -> byId("tache_WREF-38");
        $button ->click();
        #Then I should see "Historique de la tache WREF-38"
        $this -> assertNotNull($this ->byXPath("//h1[contains(.,'Historique')]"));
        #And I should see a div named "containerGraph"
        $this -> assertNotNull($this ->byXPath("//div[starts-with(@id, 'containerGraph')]"));
    }
}
