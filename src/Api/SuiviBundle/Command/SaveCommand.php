<?php

namespace Api\SuiviBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Api\SuiviBundle\Controller\SaveCenter;
use Api\SuiviBundle\Entity\Log;

/**
 * Command to save automatically tasks
 */
class SaveCommand extends ContainerAwareCommand {

    /**
     * Configure the command : php app/console apiSuivi:save
     */
    protected function configure() {
        $this
                ->setName('apiSuivi:save')
                ->setDescription('Sauvegarder les Jira dans la base de données')
        ;
    }

    /**
     * Command treatment
     * This is the same treatment as saveAction in ProjetController
     * 
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $releases = $em->getRepository('ApiSuiviBundle:ReleaseProjet')->findAll();

        $today = date('Y-m-d');
        $date = date("U", strtotime($today));

        $saveCenter = new SaveCenter();

        $saveCenter->deleteTodaySave($date, $em);

        $saveInfoRelease = array();
        foreach ($releases as $release) {
            array_push($saveInfoRelease, $saveCenter->saveReleaseTasks($release, $date, $em));
        }

        $sprints = $em->getRepository('ApiSuiviBundle:Sprint')->findAll();
        $saveInfoSprint = array();
        foreach ($sprints as $sprint) {
            array_push($saveInfoSprint, $saveCenter->saveSprintTasks($sprint, $date, $em));
        }

        $trace = $this->convertSaveInfoReleaseToTrace($saveInfoRelease)
                . "<br>"
                . $this->convertSaveInfoSprintToTrace($saveInfoSprint);

        $log = new Log();
        $log->setDate(date("U"))
                ->setTrace($trace)
                ->setTypeSave("Automatique");

        $em->persist($log);
        $em->flush();

        $output->writeln($this->convertArrayToTrace($trace));
    }

    /**
     * Convert array with release save information into string
     * 
     * @param array $saveInfoRelease
     * @return string trace
     */
    private function convertSaveInfoReleaseToTrace($saveInfoRelease) {
        $trace = "";
        //Check if the array is not empty
        if (count($saveInfoRelease) > 0) {
            //Each info in the array
            foreach ($saveInfoRelease as $tab) {
                //Check if the info is not null
                if($tab != null){
                    $trace .=
                            $tab->getAjout() .
                            " Jira ajoutées à la base de données pour la release " .
                            $tab->getReleaseProjet()->getNom() . "</br>";
                }
            }
        } else {
            $trace .= "La base de données est à jour";
        }
        return $trace;
    }

    /**
     * Convert array with sprint save information into string
     * 
     * @param type $saveInfoSprint
     * @return string
     */
    private function convertSaveInfoSprintToTrace($saveInfoSprint) {
        $trace = "";
        //Check if the array is not empty
        if (count($saveInfoSprint) > 0) {
            //Each info in the array
            foreach ($saveInfoSprint as $tab) {
                //Check if the info is not null
                if($tab != null){
                    $trace .=
                            $tab->getAjout() .
                            " Jira reliées à la base de données pour le sprint concernant la version " .
                            $tab->getSprint()->getVersion() . " du projet " . $tab->getSprint()->getProjet()->getNom() . "</br>";
                }
            }
        } else {
            $trace .= "La base de données est à jour";
        }
        return $trace;
    }

}
