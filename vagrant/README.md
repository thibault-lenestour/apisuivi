# Description
This configuration includes following software:

* PHP 5.4.19 
* MySQL 5.5.32
* GIT 1.7.9.5
* Apache 2.2.22
* Vim
* MC (Midnight commander)
* Curl
* Composer

# Usage

First you need to create vagrant VM

```
$ cd vagrant
$ vagrant up
```

While waiting for Vagrant to start up, you should add an entry into /etc/hosts file on the host machine.

```
10.0.0.200      sylius.dev
```

If necessary, change keyboard layout in the VM console
```
$ sudo dpkg-reconfigure keyboard-configuration
```

Then end sylius installation with
```
$ composer update
```

From now you should be able to access your Sylius project at [http://sylius.dev/](http://sylius.dev/)

# Troubleshooting

Using Symfony2 inside Vagrant can be slow due to synchronisation delay incurred by NFS. To avoid this, both locations have been have been moved to a shared memory segment under ``/dev/shm/sylius``.

To view the application logs, run the following commands:

```bash
$ tail -f /dev/shm/sylius/app/logs/prod.log
$ tail -f /dev/shm/sylius/app/logs/dev.log
```

# Extending a model

Adding field to a model has to be done un CoreBundle and requires theses steps :
- add field definition in Resource/config/doctrine/model/[Product].orm.xml

```xml
<field name="viewCount" column="view_count" type="integer" nullable="true"/>
```

- add Property, Accessors and Mutators Methods prototypes in Model Interface

```php
    /**
     * Get product view count.
     *
     * @return int
     */
    public function getViewCount();
    
    /**
     * Set product view count.
     *
     * @param int $viewCount
    */
    public function setViewCount($viewCount);
```

- Define model property and implement Accessors and Mutators Methods in Model

```php
    /**
     * Count visits of this product.
     *
     * @var int
     */
    protected $viewCount;

    /**
     * {@inheritdoc}
     */
    public function getViewCount()
    {
        return $this->viewCount;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setViewCount($viewCount)
    {
        $this->viewCount = $viewCount;
        
        return $this;
    }
```

- add (if necessary) the form element in the Form Type

```php
    $builder
            ->add('viewCount', 'number', array(
                'label'       => 'sylius.form.product.view_count',
            ))
    ;
```

- run the database schema update :

``` bash
app/console doctrine:schema:update
```

- and edit the twig templates in WebBundle to use the new property
